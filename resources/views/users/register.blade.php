<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>register</title>
</head>
@include('layout.head')
<body>
  <div class="card" style="width: 37rem; margin: 6% auto; box-shadow: 2px 2px 11px 3px #198754;">
    @if(session('msj'))
      <div class="alert alert-success">
        {{ session('msj') }}
      </div>
    @endif
    <div class="card-body">
      <form method="post">
        <h5 class="text-center">REGISTRATE</h5>
        <br>
        @csrf
        <div class="mb-3">
          <label for="name" class="form-label">Nombres Completos</label>
          <input class="form-control" type="text" name="name" id="name" value="{!! old('name') !!}">
          @if($errors->has('name'))
            <div class="alert alert-danger">{!! $errors->first('name') !!}</div>
          @endIf
        </div>
        <div class="mb-3">
          <label for="email" class="form-label">Email</label>
          <input class="form-control" type="email" name="email" id="email" value="{!! old('email') !!}">
          @if($errors->has('email'))
            <div class="alert alert-danger">{!! $errors->first('email') !!}</div>
          @endIf
        </div>
        <div class="mb-3">
          <label for="password" class="form-label">Contraseña</label>
          <input class="form-control" type="password" name="password" id="password">
          @if($errors->has('password'))
            <div class="alert alert-danger">{!! $errors->first('password') !!}</div>
          @endIf
        </div>
        <div class="mb-3">
          <label for="password_confirmation" class="form-label">Repita Contraseña</label>
          <input class="form-control" type="password" name="password_confirmation" id="password_confirmation">
          @if($errors->has('password_confirmation'))
            <div class="alert alert-danger">{!! $errors->first('password_confirmation') !!}</div>
          @endIf
        </div>
        <button class="btn btn-success col-12" tipe="submit">Registrarse</button>
        <br><br>
        <a style="margin: auto;"href="{{ url('/login')}}">Iniciar Sesion</a>
        <br>
      </form>
    </div>
  </div>
</body> 
</html>