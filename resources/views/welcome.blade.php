<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>welcome</title>
</head>
<body>
    <form action="/logout" method="post">
        @csrf
        <a href="#" onclick="this.closest('form').submit()">Salir</a>
    </form>
    <h1>{!! Auth::user()->name !!}</h1>
</body>
</html>