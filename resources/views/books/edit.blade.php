<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>edit book</title>
</head>
@include('layout.nav')
<body>
  <div class="card" style="width: 37rem; margin: 3% auto; box-shadow: 2px 2px 11px 3px #198754;">
    @if(session('msj'))
      <div class="alert alert-success">
        {{ session('msj') }}
      </div>
    @endif
    <div class="card-body">
      <form method="post" action="{{url('book/update')}}/{!! $book[0]->id_book !!}">
        <h5 class="text-center">Editar Libro</h5>
        <br>
        @csrf
        <div class="mb-3">
          <label for="titulo" class="form-label">Titulo</label>
          <input class="form-control" type="text" name="titulo" id="titulo" value="{!! $book[0]->titulo !!}">
          @if($errors->has('titulo'))
            <div class="alert alert-danger">{!! $errors->first('titulo') !!}</div>
          @endIf
        </div>
        <div class="mb-3">
          <label for="isbn" class="form-label">ISBN</label>
          <input class="form-control" type="text" name="isbn" id="isbn" value="{!! $book[0]->isbn !!}">
          @if($errors->has('isbn'))
            <div class="alert alert-danger">{!! $errors->first('isbn') !!}</div>
          @endIf
        </div>
        <div class="mb-3">
          <label for="año_public" class="form-label">Año Publicacion</label>
          <input class="form-control" type="date" name="año_public" id="año_public" value="{!! $book[0]->año_public !!}">
          @if($errors->has('año_public'))
            <div class="alert alert-danger">{!! $errors->first('año_public') !!}</div>
          @endIf
        </div>
        <div class="mb-3">
          <select class="form-select form-select-sm" name="user_id" id="user_id" aria-label=".form-select-sm example">
            <option value="{!! $book[0]->id_users !!}" selected>{!! $book[0]->name_user !!}</option>
              @foreach($users as $user)
               @if($book[0]->id_users != $user->id)
                <option value="{!! $user->id !!}">{!! $user->name !!}</option>
               @endIf
              @endForeach
          </select>
          @if($errors->has('user_id'))
            <div class="alert alert-danger">{!! $errors->first('user_id') !!}</div>
          @endIf
        </div>
        <button class="btn btn-success col-12" tipe="submit">Editar Libro</button>
        <br>
        <br>
      </form>
    </div>
  </div>
</body> 

<!--
<body>
  <form method="post" action="{{url('book/update')}}/{!! $book[0]->id_book !!}">
    @csrf
    <input type="text" name="titulo" id="titulo" value="{!! $book[0]->titulo !!}">
    @if($errors->has('titulo'))
      <p>{!! $errors->first('titulo') !!}</p>
    @endIf
    <br>
    <input type="text" name="isbn" id="isbn" value="{!! $book[0]->isbn !!}">
    @if($errors->has('isbn'))
      <p>{!! $errors->first('isbn') !!}</p>
    @endIf
    <br>
    <input type="date" name="año_public" id="año_public" value="{!! $book[0]->año_public !!}">
    @if($errors->has('año_public'))
      <p>{!! $errors->first('año_public') !!}</p>
    @endIf
    <br>
    <select class="form-select form-select-sm" name="user_id" id="user_id" aria-label=".form-select-sm example">
      <option value="{!! $book[0]->id_users !!}" selected>{!! $book[0]->name_user !!}</option>
        @foreach($users as $user)
         @if($book[0]->id_users != $user->id)
          <option value="{!! $user->id !!}">{!! $user->name !!}</option>
         @endIf
        @endForeach
    </select>
    @if($errors->has('user_id'))
      <p>{!! $errors->first('user_id') !!}</p>
    @endIf
    <br>
    <button type="submit">Edit Book</button>
  </form>

</body>-->
</html>