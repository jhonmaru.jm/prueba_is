<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Libros</title>
</head>
@include('layout.nav')
<body>
  <style type="text/css">
    .w-5{
      width: 50px;
    }
  </style>
  <div class="container-fluid" style="padding: 5%;">
    <h4 class="text-center">LISTADO DE LIBROS</h4>
    <br><br>
    @if(session('msjDel'))
      <div class="alert alert-success">
        {{ session('msjDel') }}
      </div>
    @endif
    <table class="table" style="box-shadow: 2px 2px 11px 3px #198754;">
      <thead>
        <tr>
          <th scope="col">#</th>
          <th scope="col">Titulo</th>
          <th scope="col">ISBN</th>
          <th scope="col">Año Publicación</th>
          <th scope="col">Creador</th>
          <th scope="col">Opciones</th>
        </tr>
      </thead>
      <tbody>
        <?php $cont=1 ?>
        @foreach($books as $book)
          <tr>
            <th scope="row">{!! $cont++ !!}</th>
            <td>{!! $book->titulo !!}</td>
            <td>{!! $book->isbn !!}</td>
            <td>{!! date("m/d/Y", strtotime($book->año_public)) !!}</td>
            <td>{!! $book->name_user !!}</td>
            <th>
              <a class="btn btn-primary col-6" href="{{ url('book/edit/') }}{!! $book->id_book !!}">Editar</a>
              <form action="{{ url('book/eliminar/') }}{!! $book->id_book !!}" method="post">
                @csrf
                @method('DELETE')
                <button class="btn btn-danger col-6" type="submit">Eliminar</button>
              </form>
            </th>
          </tr>
        @endForeach
      </tbody>
    </table>
    <nav aria-label="Page navigation example">
      <ul class="pagination">
        {{ $books->links() }}
      </ul>
    </nav>
  </div>
</body>
</html>


