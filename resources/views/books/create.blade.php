<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>create book</title>
</head>
<body>
  @if(session('msj'))
      <div class="alert alert-success">
          {{ session('msj') }}
      </div>
  @endif

  <form method="post">
    @csrf
    <input type="text" name="titulo" id="titulo" value="{!! old('titulo') !!}">
    @if($errors->has('titulo'))
      <p>{!! $errors->first('titulo') !!}</p>
    @endIf
    <br>
    <input type="text" name="isbn" id="isbn" value="{!! old('isbn') !!}">
    @if($errors->has('isbn'))
      <p>{!! $errors->first('isbn') !!}</p>
    @endIf
    <br>
    <input type="date" name="año_public" id="año_public" value="{!! old('año_public') !!}">
    @if($errors->has('año_public'))
      <p>{!! $errors->first('año_public') !!}</p>
    @endIf
    <br>
    <select class="form-select form-select-sm" name="user_id" id="user_id" aria-label=".form-select-sm example">
      <option value="" selected disabled>Seleccione un usuario</option>
        @foreach($users as $user)
          <option value="{!! $user->id !!}">{!! $user->name !!}</option>
        @endForeach
    </select>
    @if($errors->has('user_id'))
      <p>{!! $errors->first('user_id') !!}</p>
    @endIf
    <br>
    <button type="submit">Register Book</button>
  </form>

</body>
</html>