<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Login</title>
</head>
@include('layout.head')
<body>
  <div class="card" style="width: 22rem; margin: 10% auto; box-shadow: 2px 2px 11px 3px #198754;">
    @if(session('msj'))
      <div class="alert alert-danger">
        {{ session('msj') }}
      </div>
    @endif
    <div class="card-body" >
      <form method="post">
        <h5 class="text-center">INICIAR SESIÓN</h5>
        <br>
        @if($retries <= 0)
          por favor vuelva a intentarlo en {!! $seconds !!} segundos.
        @endIf
        @csrf
        <div class="mb-3">
          <label for="email" class="form-label">EMAIL</label>
          <input class="form-control" type="email" name="email" id="email" required>
        </div>
        <div class="mb-3">
          <label for="password" class="form-label">PASSWORD</label>
          <input class="form-control" type="password" name="password" id="password"required>
        </div>
        <button class="btn btn-success col-12" tipe="submit">Login</button>
        <br><br>
        <a style="margin: auto;"href="{{ url('/register')}}">Registrate</a>
        <br>
      </form>
    </div>
  </div>
</body>
</html>