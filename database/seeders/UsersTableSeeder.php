<?php

namespace Database\Seeders;

use App\Models\User;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user1 = new User;
        $user1->name       = "Jhon Marulanda";
        $user1->email      = "jhonmarulanda@hotmail.com";
        $user1->password   = '$2y$10$XSqVFDLZM/MBTGHBLou7vevHvvaE6EZ2HXLCsAMJx/koU5N73A7Rq';
        $user1->save();

        $user2 = new User;
        $user2->name       = "Maria Fernada";
        $user2->email      = "mafe@hotmail.com";
        $user2->password   = '$2y$10$XSqVFDLZM/MBTGHBLou7vevHvvaE6EZ2HXLCsAMJx/koU5N73A7Rq';
        $user2->save();

        $user3 = new User;
        $user3->name       = "Camilo Torres";
        $user3->email      = "camilo@hotmail.com";
        $user3->password   = '$2y$10$XSqVFDLZM/MBTGHBLou7vevHvvaE6EZ2HXLCsAMJx/koU5N73A7Rq';
        $user3->save();

        $user4 = new User;
        $user4->name       = "Paula Andres";
        $user4->email      = "paula@hotmail.com";
        $user4->password   = '$2y$10$XSqVFDLZM/MBTGHBLou7vevHvvaE6EZ2HXLCsAMJx/koU5N73A7Rq';
        $user4->save();
    }
}
