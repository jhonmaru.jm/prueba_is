<?php

namespace Database\Seeders;

use App\Models\Book;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class BooksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $book1 = new Book;
        $book1->titulo     = "La utlima Huella";
        $book1->isbn       = "123-123-1234-22-3";
        $book1->año_public = "2022-12-21";
        $book1->user_id    = 1;
        $book1->save();

        $book2 = new Book;
        $book2->titulo     = "El avion";
        $book2->isbn       = "123-123-1234-22-4";
        $book2->año_public = "2021-12-21";
        $book2->user_id    = 2;
        $book2->save();

        $book3 = new Book;
        $book3->titulo     = "La vaca";
        $book3->isbn       = "123-123-1234-21-3";
        $book3->año_public = "1998-12-20";
        $book3->user_id    = 3;
        $book3->save();

        $book4 = new Book;
        $book4->titulo     = "Las aves";
        $book4->isbn       = "123-123-0234-22-3";
        $book4->año_public = "2012-12-21";
        $book4->user_id    = 4;
        $book4->save();

        $book5 = new Book;
        $book5->titulo     = "Ciencias Sociales";
        $book5->isbn       = "123-123-1254-22-3";
        $book5->año_public = "2022-09-21";
        $book5->user_id    = 1;
        $book5->save();

        $book6 = new Book;
        $book6->titulo     = "Futbol";
        $book6->isbn       = "123-173-1234-22-4";
        $book6->año_public = "2011-12-21";
        $book6->user_id    = 2;
        $book6->save();

        $book7 = new Book;
        $book7->titulo     = "Farandula";
        $book7->isbn       = "193-123-1234-21-3";
        $book7->año_public = "1898-12-20";
        $book7->user_id    = 3;
        $book7->save();

        $book8 = new Book;
        $book8->titulo     = "La ciencia";
        $book8->isbn       = "123-923-0234-22-3";
        $book8->año_public = "2012-12-21";
        $book8->user_id    = 4;
        $book8->save();

        $book9 = new Book;
        $book9->titulo     = "Alumbra";
        $book9->isbn       = "223-123-1264-22-3";
        $book9->año_public = "2012-12-21";
        $book9->user_id    = 1;
        $book9->save();

        $book10 = new Book;
        $book10->titulo     = "Motos";
        $book10->isbn       = "123-173-7234-22-4";
        $book10->año_public = "2011-12-21";
        $book10->user_id    = 2;
        $book10->save();

        $book11 = new Book;
        $book11->titulo     = "Lq vida";
        $book11->isbn       = "193-113-1234-21-3";
        $book11->año_public = "1998-12-20";
        $book11->user_id    = 3;
        $book11->save();

        $book12 = new Book;
        $book12->titulo     = "El exito";
        $book12->isbn       = "123-963-0234-22-3";
        $book12->año_public = "2013-12-21";
        $book12->user_id    = 4;
        $book12->save();
    }
}
