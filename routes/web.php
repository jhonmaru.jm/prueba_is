<?php

use App\Http\Controllers\loginController;
use App\Http\Controllers\userController;
use App\Http\Controllers\libroController;

use Illuminate\Support\Facades\Route;

/**
 * Autor: Jhon Marulada
 * Description: Routes
 */

//Routes login
Route::get('/login', [loginController::class, 'index'])->name('login')->middleware(['guest', 'throttle:login']);
Route::post('/login', [loginController::class, 'login']);
Route::post('/logout', [loginController::class, 'logout']);

//User registration paths
Route::get('/register', [userController::class, 'index']);
Route::post('/register', [userController::class, 'create']);

//Routes CRUD for books
Route::group(['prefix'=>'book', 'middleware'=>'auth'], function(){

  Route::get('/', [libroController::class, 'index']);
  Route::get('/register', [libroController::class, 'create']);
  Route::post('/register', [libroController::class, 'store']);
  Route::get('/edit{id}', [libroController::class, 'edit']);
  Route::post('/update/{id}', [libroController::class, 'update']);
  Route::delete('/eliminar{id}', [libroController::class, 'destroy']);
});