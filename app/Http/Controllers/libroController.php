<?php

namespace App\Http\Controllers;

use App\Models\Book;
use App\Models\User;

use Illuminate\Http\Request;

class libroController extends Controller
{
  
  /**
  * Autor: Jhon Marulada
  * Description: Controller for login module 
  */
  
  public function index(){
    
    $books = Book::join('users', 'users.id', '=', 'books.user_id')
      ->select('books.id AS id_book', 'titulo', 'isbn', 'año_public', 'users.name AS name_user')
      ->paginate(10);

    return view('books.index', compact('books'));
  }

  public function create(){

    $users = User::select('id', 'name')->get();

    return view('books.create', compact('users'));
  }

  public function store(Request $request){
    $data = request()->validate([
      'titulo'     => 'required|max:500',
      'isbn'       => "required|regex:/^(?=(?:\D*\d){10}(?:(?:\D*\d){3})?$)[\d-]+$/|unique:books,isbn,{$request->id}",
      'año_public' => 'required|date_format:Y-m-d',
      'user_id'    => 'required'
    ],[
      'required'      => 'Este campo es obligatorio.',
      'titulo.max'    => 'Solo se permite un maximo de 250 caracteres.',
      'isbn.regex'    => 'Este campo debe cumplir con el siguiente formato: xxx-xxx-xxxx-xx-x.',
      'isbn.unique'   => 'Este codigo ya exite.',
      'año_public.date_format' => 'Formato de fecha incorrecto.'.$request->año_public,
    ]);

    $book = new Book;

    $book->titulo     = $request->titulo;
    $book->isbn       = $request->isbn;
    $book->año_public = $request->año_public;
    $book->user_id    = $request->user_id;

    if($book->save() == true){
      return back()->with('msj', 'El libro se creo exitosamente');
    } 
    return back()->with('msj', 'Error al crear libro.');
  }

  public function edit($id){

    $users = User::select('id', 'name')->get();

    $book = Book::join('users', 'users.id', '=', 'books.user_id')
      ->select('books.id AS id_book', 'titulo', 'isbn', 'año_public', 'users.name AS name_user', 'users.id AS id_users')
      ->where('books.id', $id)
      ->get();

    return view('books.edit', compact('book', 'users'));
  }

  public function update(Request $request, $id){

    $data = request()->validate([
      'titulo'     => 'required|max:500',
      'isbn'       => 'required|regex:/^(?=(?:\D*\d){10}(?:(?:\D*\d){3})?$)[\d-]+$/|unique:books,isbn,'.$request->isbn.',isbn',
      'año_public' => 'required|date_format:Y-m-d',
      'user_id'    => 'required'
    ],[
      'required'      => 'Este campo es obligatorio.',
      'titulo.max'    => 'Solo se permite un maximo de 250 caracteres.',
      'isbn.regex'    => 'Este campo debe cumplir con el siguiente formato: xxx-xxx-xxxx-xx-x.',
      'isbn.unique'   => 'Este codigo ya exite.',
      'año_public.date_format' => 'Formato de fecha incorrecto.'.$request->año_public,
    ]);

    $book = Book::select('id', 'titulo', 'isbn', 'año_public', 'user_id')
    ->where('id', $id)
    ->update(['titulo'=>$request->titulo, 'isbn' => $request->isbn, 'año_public'=> $request->año_public, 'user_id'=> $request->user_id]);
   
    return back()->with('msj', 'El libro se edito correctamente');
  }

  public function destroy($id){

    $book = Book::find($id);
    $book->delete();

    return back()->with('msjDel', 'El libro se elimino correctamente');
  }

  
}
