<?php

namespace App\Http\Controllers;

use RateLimiter;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class loginController extends Controller
{

  /**
  * Autor: Jhon Marulada
  * Description: Controller for login module 
  */
  
  public function index(Request $request){
    $key = 'login.'.$request->ip();
    
    return view('auth.login', ['key'=>$key, 
      'retries'=>RateLimiter::retriesLeft($key, 5),
      'seconds'=>RateLimiter::availableIn($key),
    ]);
  }

  public function login(Request $request){

    $credenciales = request()->only('email', 'password');

    if(Auth::attempt($credenciales)){

      request()->session()->regenerate();
      RateLimiter::clear('login.'.$request->ip());
      return redirect('book/');
    }
    return back()->with('msj', 'Credenciales Incorrectas');
  }

  public function logout(Request $request){
    
    Auth::logout();
    
    $request->session()->invalidate(); 
    $request->session()->regenerateToken();

    return redirect('/login');
  }

}
