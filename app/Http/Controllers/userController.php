<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Services\PayUService\Exception;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;


class userController extends Controller
{

  public function index(){
    
    return view('users.register');
  }

  public function create(Request $request){

    $data = request()->validate([
      'name'     => 'required|regex:/^[a-zA-Z]+(\s*[a-zA-Z]*)*[a-zA-Z]+$/|max:250',
      'email'    => "required|regex:/\S+@\S+\.\S+/|unique:users,email,{$request->id}|max:300",
      'password' => 'required|min:8|regex:/[A-Z]/|max:600|confirmed',
      'password_confirmation' => 'required'
    ],[
      'required'      => 'Este campo es obligatorio.',
      'name.regex'    => 'Formato invalido, solo se permiten letras.',
      'name.max'      => 'Solo se permite un maximo de 250 caracteres.',
      'email.regex'   => 'El formato de correo es invalido.',
      'email.unique'  => 'Este correo ya exite.',
      'password.min'  => 'Este campo debe tener almenos 8 caracteres.',
      'password.regex'=> 'Este campo debe tener almenos una mayuscula.',
      'password.max'  => 'Solo se permite un maximo de 600 caracteres.',
      'password.confirmed' => 'Las contraseña no coinciden.',
    ]);

    $user = new User;

    $user->name     = $request->name;
    $user->email    = $request->email;
    $user->password = Hash::make($request->password);
    $user->save();

    return redirect('/register')->with('msj', 'Perfil creado con exito!');
  }
}

